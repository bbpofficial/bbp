(function () {
  'use strict';

  angular
    .module('core')
    .factory('booksDataService', booksDataService);

  booksDataService.$inject = [/*Example: '$state', '$window' */];

  function booksDataService(/*Example: $state, $window */) {
    var service = {};
    var books = [];
    var sellersFilter, priceRangeFilter, inStockFilter;
    //sample data
    var sampleBooks = [{
        isbn10: 1,
        isbn13: 1,
        title: 'Sample book #1',
        imageURL: 'http://books-a2.infibeam.com/img/bbcfedd5/018e2/63/815/P-M-B-9789382563815.jpg',
        bestPrice: 991,
        relevanceScore: Math.floor(Math.random() * 10) + 1,
        matchesCount: 2,
        pricingMap: [{
            seller: 'Amazon.in',
            price: 991,
            position: 1,
            cheapest: true,
            inStock: true
        }, {
            seller: 'Flipkart.com',
            price: 999,
            position: 1,
            cheapest: false,
            inStock: true
        }]
    }, {
        isbn10: 2,
        isbn13: 2,
        title: 'Sample book #2',
        imageURL: 'http://books-a2.infibeam.com/img/bbcfedd5/018e2/63/815/P-M-B-9789382563815.jpg',
        bestPrice: 992,
        relevanceScore: Math.floor(Math.random() * 10) + 1,
        matchesCount: 2,
        pricingMap: [{
            seller: 'Snapdeal.com',
            price: 992,
            position: 1,
            cheapest: true,
            inStock: false
        }, {
            seller: 'Flipkart.com',
            price: 999,
            position: 1,
            cheapest: false,
            inStock: false
        }]
    }, {
        isbn10: 3,
        isbn13: 3,
        title: 'Sample book #3',
        imageURL: 'http://books-a2.infibeam.com/img/bbcfedd5/018e2/63/815/P-M-B-9789382563815.jpg',
        bestPrice: 992,
        relevanceScore: Math.floor(Math.random() * 10) + 1,
        matchesCount: 2,
        pricingMap: [{
            seller: 'Snapdeal.com',
            price: 992,
            position: 1,
            cheapest: true,
            inStock: true
        }, {
            seller: 'Amazon.in',
            price: 999,
            position: 1,
            cheapest: false,
            inStock: true
        }]
    }];

    service.getData = function(query, booksArray, sellersFilterLive, priceRangeFilterLive, inStockFilterLive) {
      books = booksArray;
      sellersFilter = sellersFilterLive;
      priceRangeFilter = priceRangeFilterLive;
      inStockFilter = inStockFilterLive;
        
      //establish socket connections to retrieve data with the mentioned parameters
      return books;
    };
      
    service.getSampleData = function() {
      service.books = sampleBooks;
    };
      
    service.books = books;

    return service;
  }
})();