(function() {
  'use strict';

  angular
    .module('core')
    .controller('SearchController', SearchController)
    .constant("_", window._);

  SearchController.$inject = ['$scope', '$state', '$stateParams', 'Socket', '$timeout'];

  function SearchController($scope, $state, $stateParams, Socket, $timeout) {
    var vm = this;
    var sellersFilter =  {options: []};
    var inStockFilter = {IsIncluded: false};
    var bookName = $stateParams.bookName;
    var liveBooks = [];
    var priceRangeFilter = 
        {
            min: -1, 
            max: -1, 
            options: {
                floor: 0, 
                ceil: 0,
                translate: function(value) {
                  return 'Rs.' + value;
                }
            }
        };
    
    $scope.Books = liveBooks;
    $scope.q = bookName;
    
    // Make sure the Socket is connected
    if (!Socket.socket) {
      Socket.connect();
    }

    Socket.on('error', function (data) {
        console.error(data || 'error');
    });
      
    // Tell NodeJS server to crawl for the current search term
    runQueryOnServer();
      
    Socket.on("results:book", function(response) {
        var query = response.q;
        var books = response.products;
        var seller = response.seller;
        if(books === null || books.length === 0) {
            var existingOption = _.find(sellersFilter.options, {
                value: seller
            });
            if (existingOption) {
                if(existingOption.loading)
                    existingOption.loading = false;
            }
            return;
        }
        console.info("Got data from seller", seller, books);
        _.each(books, function(book) {
            var isbn, matchISBN;
            //Using ISBN13 as standard
            /*if(book.isbn10 !== -1 && book.isbn10 !== "-1") {
                isbn = book.isbn10;
                matchISBN = _.some(liveBooks, {
                   isbn10: isbn
                });
            } else */
            isbn = book.isbn13;
            matchISBN = _.some(liveBooks, {
               isbn13: isbn
            });
            
            if(matchISBN) {
                // Book exists in our map
                // Add the data from `book` into pricingMap
                /*if(isbn.length === 10) {
                    existingBook = _.find(liveBooks, _.matchesProperty('isbn10', isbn));
                } else if(isbn.length === 13) { */
                var existingBook = _.find(liveBooks, _.matchesProperty('isbn13', isbn));
                console.log("Existing book ->", existingBook);
                var index = _.findIndex(liveBooks, existingBook);
                
                var newPricingEntry = {
                    seller: response.seller,
                    mrp: parseInt(book.pricingMap.mrp),
                    price: parseInt(book.pricingMap.price),
                    position: parseInt(book.pricingMap.position),
                    inStock: (book.pricingMap.inStock == "true" || book.pricingMap.inStock == true),
                    rating: parseInt(book.pricingMap.rating),
                    url: book.pricingMap.url
                };
                
                existingBook.pricingMap.push(newPricingEntry);
                existingBook.matchesCount = existingBook.pricingMap.length;
                existingBook.relevanceScore =  calculateRelevance(existingBook);
                
                liveBooks.splice(index, 1, existingBook);
            } else {
                // Book does not exist in our pricing map
                // Add new entry into it
                var newBook = {
                    isbn10: book.isbn10,
                    isbn13: book.isbn13,
                    title: book.title,
                    imageURL: book.imageUrl,
                    bestPrice: parseInt(book.pricingMap.price),
                    relevanceScore: 0,
                    matchesCount: 1,
                    pricingMap: [{
                        seller: response.seller,
                        mrp: parseInt(book.pricingMap.mrp),
                        price: parseInt(book.pricingMap.price),
                        position: parseInt(book.pricingMap.position),
                        inStock: (book.pricingMap.inStock == "true" || book.pricingMap.inStock == true),
                        rating: parseInt(book.pricingMap.rating),
                        url: book.pricingMap.url
                    }]
                };
                
                newBook.relevanceScore = calculateRelevance(newBook);
                liveBooks.push(newBook);
            }
        });
        $scope.Books = liveBooks;
        updateFilters();
    });
    
    updateFilters();
    addDefaultSellers();
    
    function updateFilters() {
        //Reset all counts
        _.each(sellersFilter.options, function(existingOption) {
            existingOption.count = 0;
        });
        
        _.each(liveBooks, function(book) {
          _.each(book.pricingMap, function(bookStore) {
            //var intBookPrice = bookStore.price.replace("Rs. ", "").replace(",", "");
            bookStore.price = parseInt(bookStore.price);
              //Price Range Filter
            if(priceRangeFilter.min === -1 || priceRangeFilter.min > bookStore.price) {
                priceRangeFilter.options.floor = bookStore.price;
                priceRangeFilter.min = bookStore.price;
            }
            if(priceRangeFilter.max === -1 || priceRangeFilter.max < bookStore.price) {
                priceRangeFilter.options.ceil = bookStore.price;
                priceRangeFilter.max = bookStore.price;
            }

            //Sellers Filter
            var existingOption = _.find(sellersFilter.options, {
                value: bookStore.seller
            });
            if (existingOption) {
                existingOption.count += 1;
                if(existingOption.loading)
                    existingOption.loading = false;
            } else {
                sellersFilter.options.push({
                    value: bookStore.seller,
                    count: 1,
                    IsIncluded: true,
                    loading: false
                });
            }
          });
        });
    }
    
    function addDefaultSellers() {
        var sellers = ["Flipkart.com", /*"Snapdeal.com",*/ "Amazon.in", "Infibeam.com", "Bookadda.com", "Sapnaonline.com"];
        _.each(sellers, function(seller) {
           sellersFilter.options.push({
                value:seller,
                count: 0,
                IsIncluded: true,
                loading: true
            }); 
        });
    }
      
    function runSearch() {
      var query = $scope.q;

      window.location.href = "//bestbooksprices.com/search/" + query;
    }
      
    function runQueryOnServer() {
        var query = $scope.q;
        
        Socket.emit('search:book', {q: query});
        console.log("Requested server for", query);
    }

    function calculateRelevance(book) {        
        var pricingMap = book.pricingMap; // c
        var matchesCount = book.pricingMap.length; // f
        
        var relevanceScore = 0;
        var positionScore = 0;
        
        for (var bookStoreIndex in pricingMap) {
            positionScore += parseInt(pricingMap[bookStoreIndex].position) || 0;
        }
        var positionScoreAverage = positionScore / matchesCount;
        relevanceScore = 1 / positionScoreAverage + (matchesCount - 1) / 2;
        
        return relevanceScore;
    }
    
    // Remove the event listener when the controller instance is destroyed
    $scope.$on('$destroy', function () {
      Socket.removeListener('error');
      Socket.removeListener('results:book');
    });

    $scope.runSearch = runSearch;

    $scope.SellersFilter = sellersFilter;
    $scope.PriceRangeFilter = priceRangeFilter;    
    $scope.InStockFilter = inStockFilter;
    
    $scope.responsiveShowFilters = false;
    $scope.toggleFilters = function() {
        $scope.responsiveShowFilters = !$scope.responsiveShowFilters;
    };
  }
})();