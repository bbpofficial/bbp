(function() {
  'use strict';

  angular
    .module('core')
    .controller('AboutController', AboutController);

  AboutController.$inject = ['$scope'];

  function AboutController($scope) {
    var vm = this;

      
    $scope.description = "Meta description";
    // About controller logic
    // ...

    init();

    function init() {
    }
  }
})();
