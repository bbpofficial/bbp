'use strict';

angular.module('core').controller('HomeController', ['$scope', '$state',
  function ($scope, $state) {
    function runSearch() {
      var query = $scope.q;

      $state.go('search', { bookName: query });
    }

    $scope.runSearch = runSearch;
  }
]);
