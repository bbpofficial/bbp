(function() {
  'use strict';

  angular
    .module('core')
    .controller('BookController', BookController);

  BookController.$inject = ['$scope', '$state', '$stateParams', 'Socket'];

  function BookController($scope, $state, $stateParams, Socket) {
    var vm = this;
    var isbn = (isbn) ? isbn : $stateParams.isbn;
    
    // Make sure the Socket is connected
    if (!Socket.socket) {
      Socket.connect();
    }

    Socket.on('error', function (data) {
        console.error(data || 'error');
    });

    Socket.on('response:book', function(book) {
        //console.log("Received book response: ", book);
        var totalRating = 0;
        var totalStores = 0;
        book.pricingMap.forEach(function(bookStore) {
            if (bookStore.rating > 0) {
                totalRating += bookStore.rating;
                totalStores++;
            }
        });
        totalRating = (totalRating / totalStores) * 20; //100% scale
        $scope.totalRating = totalRating;
        $scope.book = book;
    });
     
    Socket.on("latestPrice:book", function(bookStore) {
        var book = $scope.book;
        for(var i = 0; i < $scope.book.pricingMap.length; i++) {
            if($scope.book.pricingMap[i].seller == bookStore.seller) {
                if(bookStore.price !== -1) {
                    $scope.book.pricingMap[i].price = bookStore.price;
                }
                $scope.reloadingPrices--;
                break;
            }
        }
    });
    
    init();

    function init() {
        console.log("Requesting book with isbn", isbn);
        Socket.emit('request:book', {isbn: isbn});
    }
      
      
    // Remove the event listener when the controller instance is destroyed
    $scope.$on('$destroy', function () {
      Socket.removeListener('error');
      Socket.removeListener('response:book');
    });
    
    function runSearch() {
      var query = $scope.q;

      $state.go('search', { bookName: query });
    }
      
    function fetchLatestPrices() {
        //Make sure previous requests are completed
        if($scope.reloadingPrices > 0) {
            return;
        }
        Socket.emit("prices:book", {book: $scope.book});
        console.log("fetching latest prices!");
        $scope.reloadingPrices = $scope.book.pricingMap.length; // Total number of sellers
    }
    
    $scope.book = {};
    $scope.runSearch = runSearch;
    $scope.reloadingPrices = 0;
    $scope.fetchLatestPrices = fetchLatestPrices;
  }
})();