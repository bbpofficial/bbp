(function () {
  'use strict';

  angular
    .module('core')
    .directive('searchBar', searchBar);

  searchBar.$inject = [/*Example: '$state', '$window' */];

  function searchBar(/*Example: $state, $window */) {
    return {
      restrict: 'A',
      link: function(scope, element, attrs) {
        angular.element(element).autocomplete("//suggestqueries.google.com/complete/search?client=books&ds=bo");
      }
    };
  }
})();