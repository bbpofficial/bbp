(function () {
  'use strict';

  angular
    .module('core')
    .filter('slugify', slugify);

  slugify.$inject = [/*Example: '$state', '$window' */];

  function slugify(/*Example: $state, $window */) {
    return function (input) {
      input = input || '';  
      input = input.replace(/ /g, '-');
      input = input.toLowerCase();
      return input;
    };
  }
})();