(function () {
  'use strict';

  angular
    .module('core')
    .filter('extractBooks', extractBooks)
    .constant("_", window._);

  extractBooks.$inject = [/*Example: '$state', '$window' */];

  function extractBooks(/*Example: $state, $window */) {
    return function(products, sellersFilterOptions, priceRange, includeOutStock, scope) {

        var filtered = [];
        
        //Active sellers
        var activeSellersFilters = _.filter(sellersFilterOptions, function(sellerOption) {
            return sellerOption.IsIncluded;
        });
        
        _.each(products, function(product) {
            var matches = 0;
            var bestPrice = -1;
            var includeProduct = false; //Assume it can not be included unless it fits a price range
            _.each(product.pricingMap, function(bookStore) {
                //Price Filter
                if(bookStore.price >= priceRange.min && bookStore.price <= priceRange.max) {
                    //Book has result that fits in the price range
                    
                    //Sellers Filter
                    if(_.some(activeSellersFilters, {
                        value: bookStore.seller
                    })) {
                        if(bookStore.inStock || includeOutStock) {
                            includeProduct = true;
                            matches++;
                            if(bestPrice === -1 || bestPrice > bookStore.price) {
                                bestPrice = bookStore.price;
                            }
                        }
                    }
                }
            });
            
            if (includeProduct && activeSellersFilters.length > 0) {
                product.bestPrice = bestPrice;
                product.matchesCount = matches;
                
                filtered.push(product);
            }
        });
        
        
        //In Stock filter
        
        return filtered;
    };
  }
})();
