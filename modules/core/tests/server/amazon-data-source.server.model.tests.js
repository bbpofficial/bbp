'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  AmazonDataSource = mongoose.model('AmazonDataSource');

/**
 * Globals
 */
var user, amazonDataSource;

/**
 * Unit tests
 */
describe('Amazon data source Model Unit Tests:', function() {
  beforeEach(function(done) {
    user = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'test@test.com',
      username: 'username',
      password: 'password'
    });

    user.save(function() { 
      amazonDataSource = new AmazonDataSource({
        // Add model fields
        // ...
      });

      done();
    });
  });

  describe('Method Save', function() {
    it('should be able to save without problems', function(done) {
      return amazonDataSource.save(function(err) {
        should.not.exist(err);
        done();
      });
    });
  });

  afterEach(function(done) { 
    AmazonDataSource.remove().exec();
    User.remove().exec();

    done();
  });
});
