'use strict';

var  mongoose = require('mongoose'),
  Books = mongoose.model('Book');

/**
 * Render the main application page
 */
exports.renderIndex = function (req, res) {
  res.render('modules/core/server/views/index', {
    
  });
};

/**
 * Render the book detail page
 */
exports.renderBook = function (req, res) {
    var originalUrl = req.originalUrl.split("/");
    var isbn = originalUrl[originalUrl.length - 1];
    isbn = (isbn) ? isbn : originalUrl[originalUrl.length - 2];
    Books.findOne({ isbn13: isbn }, function(error, book) {
        if(error) {
            console.error(error);
        }
        var totalRating = 0;
        var totalStores = 0;
        book.pricingMap.forEach(function(bookStore) {
            if (bookStore.rating > 0) {
                totalRating += bookStore.rating;
                totalStores++;
            }
        });
        totalRating = (totalRating / totalStores) * 20; //100% scale
        res.render('modules/core/server/views/book', {
            book: book,
            isbn: isbn,
            totalRating: totalRating,
            showDescription: (book.description) ? true : false
        });
    });
};


/**
 * Render the server error page
 */
exports.renderServerError = function (req, res) {
  res.status(500).render('modules/core/server/views/500', {
    error: 'Oops! Something went wrong...'
  });
};

/**
 * Render the server not found responses
 * Performs content-negotiation on the Accept HTTP header
 */
exports.renderNotFound = function (req, res) {

  res.status(404).format({
    'text/html': function () {
      res.render('modules/core/server/views/404', {
        url: req.originalUrl
      });
    },
    'application/json': function () {
      res.json({
        error: 'Path not found'
      });
    },
    'default': function () {
      res.send('Path not found');
    }
  });
};