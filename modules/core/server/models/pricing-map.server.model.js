'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * PricingMap Schema
 */
var PricingMapSchema = new Schema({
    seller: String,
    mrp: Number,
    price: Number,
    position: Number,
    inStock: Boolean,
    rating: Number,
    url: String,
    updated: { type: Date, default: Date.now },
    offer: String
});

mongoose.model('PricingMap', PricingMapSchema);
