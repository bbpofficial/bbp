// called when any scraper returns data

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * PricingMap Schema
 */
var PricingMapSchema = new Schema({
    seller: String,
    mrp: Number,
    price: Number,
    position: Number,
    inStock: Boolean,
    rating: Number,
    url: String,
    updated: { type: Date, default: Date.now },
    offer: String
});
/**
 * Book Schema
 */
var BookSchema = new Schema({
    title: String,        
    imageUrl: String,
    bestPrice: Number,
    author: String,
    description: String,
    relevanceScore: Number,
    matchesCount: Number,
    isbn10: String,
    isbn13: String,
    pricingMap: [PricingMapSchema]
});

mongoose.model('Book', BookSchema);