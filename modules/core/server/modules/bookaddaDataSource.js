'use strict';

var nPromise = require('promise');
var PythonShell = require('python-shell');

var options = {
  mode: 'text',
  pythonPath: 'python',
  pythonOptions: ['-u'],
  scriptPath: '/home/ec2-user/scrapers/',
  args: ['search term']
};

var products;

exports.getData = function(term) {
    return new nPromise( function(resolve, reject) {
        options.args = [term];
        
        PythonShell.run('bookaddaScraper.py', options, function (err, products) {
          if (err) throw err;
            
          var jsonProducts = JSON.parse(products);
          saveOrUpdate(products);
          resolve(jsonProducts);
        });       
    });
};


exports.getLatestPrice = function(url) {
    return new nPromise( function(resolve, reject) {
        options.args = [url];
        
        PythonShell.run('bookaddaPriceScraper.py', options, function (err, price) {
          if (err) throw err;
          price = parseInt(price);
          resolve(price);
        });
                
    });
};

function saveOrUpdate(products) {
    var insertOptions = {
        mode: options.mode,
        pythonPath: options.pythonPath,
        pythonOptions: options.pythonOptions,
        scriptPath: '/home/ec2-user/inserters/',
        args: [ products ]
    }
    return new nPromise(function(resolve, reject) {
        
        PythonShell.run("saveAndUpdate.py", insertOptions, function(err, response) {
            if(err) throw err;
            
            console.log("Find and Update response", response);
            resolve(response);
        });
    });
}