'use strict';

var util = require('util');
var OperationHelper = require('apac').OperationHelper;
var parseString = require('xml2js').parseString;

var opHelper = new OperationHelper({
    awsId:     'AKIAJ3DXTSH6JPYML4ZQ',
    awsSecret: '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',
    assocId:   'bestbookspric-21',
    locale: 'IN'
});

var nPromise = require('promise');
var PythonShell = require('python-shell');

var options = {
  mode: 'text',
  pythonPath: 'python',
  pythonOptions: ['-u'],
  scriptPath: '/home/ec2-user/scrapers/',
  args: ['search term']
};

var products;

exports.getData = function(search_term) {
    return new Promise( function(resolve, reject) {
        products = opHelper.execute('ItemSearch', {
                                'SearchIndex': 'Books',
                                'Keywords': search_term,
                                'ResponseGroup': 'ItemAttributes,Offers,Images',
                                'Condition': 'New'
                            })
                            .then(function(response) {
                                parseString(response.responseBody, function(err, result) {
                                    var res = result.ItemSearchResponse;
                                    var items = res.Items[0].Item;
                                    
                                    var searchProducts = [];
                                    var position = 1;
                                    items.forEach(function(item) {
                                        try {
                                            var itemAttributes = item.ItemAttributes[0];
                                            var price = parseInt(item.OfferSummary[0].LowestNewPrice[0].Amount[0] / 100); //Last two digits are decimal places
                                            if(itemAttributes.ListPrice) {
                                                var mrp = parseInt(itemAttributes.ListPrice[0].Amount / 100); //Last two digits are decimal places
                                            } else {
                                                var mrp = price;
                                            }
                                            var product = {
                                                isbn10: -1,
                                                isbn13: -1,
                                                title: itemAttributes.Title[0],
                                                author: itemAttributes.Author[0],
                                                imageUrl: item.MediumImage[0].URL[0],
                                                bestPrice: price,
                                                pricingMap: {
                                                    seller: "Amazon.in",
                                                    mrp: mrp,
                                                    price: price,
                                                    position: position,
                                                    inStock: true, // We specify to amazon to return only in stock items
                                                    rating: 0,  //Amazon doesn't provide rating with API
                                                    url: item.DetailPageURL[0]
                                                }
                                            };
                                            itemAttributes.ISBN.forEach(function(isbn) {
                                                if(isbn.toString().length == 10) {
                                                    product.isbn10 = isbn;
                                                } else if(isbn.toString().length == 13) {
                                                    product.isbn13 = isbn;
                                                }
                                            });
                                            if(product.isbn13 == -1) {
                                                product.isbn13 = isbn10Toisbn13(product.isbn10);
                                            }
                                            searchProducts.push(product);
                                        } catch(exception) {
                                            console.log("Caught exception while processing amazon:", exception);
                                        }
                                        position++;
                                    });
                                    saveOrUpdate(JSON.stringify(searchProducts));
                                    resolve(searchProducts);
                                });
                            })
                            .catch(function(err) {
                                //console.error("Something went wrong! ", err);
                                reject(err);
                            });
    });
};

exports.getLatestPrice = function(url) {
    return new nPromise( function(resolve, reject) {
        options.args = [url];
        
        PythonShell.run('amazonPriceScraper.py', options, function (err, price) {
          if (err) throw err;
          
          price = parseInt(price);
          
          resolve(price);
        });
                
    });
};

function isbn10Toisbn13(isbn10) {
    var chars = isbn10.split("");
    
    chars.unshift("9", "7", "8");
    chars.pop();
    
    var i = 0;
    var sum = 0;
    for(i = 0; i < 12; i++) {
        sum += chars[i] * ((i % 2) ? 3 : 1);
    }
    
    var checkDigit = (10 - (sum % 10)) % 10;
    chars.push(checkDigit);
    
    var isbn13 = chars.join("");
    return isbn13;
}

function saveOrUpdate(products) {
    var insertOptions = {
        mode: options.mode,
        pythonPath: options.pythonPath,
        pythonOptions: options.pythonOptions,
        scriptPath: '/home/ec2-user/inserters/',
        args: [ products ]
    }
    return new nPromise(function(resolve, reject) {
        
        PythonShell.run("saveAndUpdate.py", insertOptions, function(err, response) {
            if(err) throw err;
            
            console.log("Find and Update response", response);
            resolve(response);
        });
    });
}