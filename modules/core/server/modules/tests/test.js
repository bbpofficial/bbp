// called when any scraper returns data

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * PricingMap Schema
 */
var PricingMapSchema = new Schema({
    seller: String,
    mrp: Number,
    price: Number,
    position: Number,
    inStock: Boolean,
    rating: Number,
    url: String,
    updated: { type: Date, default: Date.now },
    offer: String
});
/**
 * Book Schema
 */
var BookSchema = new Schema({
    title: String,        
    imageUrl: String,
    bestPrice: Number,
    author: String,
    description: String,
    relevanceScore: Number,
    matchesCount: Number,
    isbn10: String,
    isbn13: String,
    pricingMap: [PricingMapSchema]
});

// Custom Methods

BookSchema.methods.saveOrUpdate = function(){
    
    var currentBook = this;
    Book.find({ isbn13: "9780702030642"}, function(err, doc){
        console.log(doc);
    });
    return;
    console.log("Finding isbn13 and seller matching:", currentBook.isbn13, currentBook.pricingMap[0].seller);
    // UPDATE PRICING MAP DETAILS FOR ISBN WITH CURRENT SELLER
    Book.find(
        { isbn13: currentBook.isbn13, seller: currentBook.pricingMap[0].seller }, function(err, doc) {
        if (err) throw err;
        // Book's match found in database
        if(doc.length) {
            console.log("Found existing entry in pricing map!", book);
            // update existing price of seller 
            var conditions = { seller: currentBook.pricingMap[0].seller };
            var update = { 
                $set: { 
                    "pricingMap.$.price": currentBook.pricingMap[0].price, 
                    "pricingMap.$.inStock": currentBook.pricingMap[0].inStock, 
                    "pricingMap.$.rating": currentBook.pricingMap[0].rating 
                }};
            var options = { upsert: true }; // adds new entry if doc doesnt exist
            model.update(conditions, update, options, callback);
        } else {
            console.log("Didn't find existing entry in any pricing map, adding a new entry!");
            // INSERT NEW ENTRY IN THE PRICING MAP FOR EXISTING ENTRY IN DB
            Book.update(
                {"isbn13": currentBook.isbn13},
                {"$push": // will only add to the end of array if value is not present
                    {
                        // add a new object
                        "pricingMap.seller": currentBook.pricingMap[0].seller,
                        "pricingMap.mrp": currentBook.pricingMap[0].mrp,
                        "pricingMap.price": currentBook.pricingMap[0].price,
                        "pricingMap.position": currentBook.pricingMap[0].position,
                        "pricingMap.inStock": currentBook.pricingMap[0].inStock,
                        "pricingMap.rating": currentBook.pricingMap[0].rating,
                        "pricingMap.url": currentBook.pricingMap[0].url,
                        "pricingMap.updated": currentBook.pricingMap[0].updated,
                        "pricingMap.offer": currentBook.pricingMap[0].offer
                    }
                },
                false, function(err, numAffected) {
                    /*if(err) throw err;
                    // Book doesn't exist in database
                    if(numAffected === 0) {
                        // ADD NEW ENTRY    
                        currentBook.save(function(err, doc){
                            if (err) throw err;
                            
                            console.log(doc);
                        });    
                    }*/
                }
            );
        }   
    });
};

var Book = mongoose.model('Book', BookSchema);
module.exports = Book;