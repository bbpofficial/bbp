// called when any scraper returns data

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var db = mongoose.connection;
db.on('error', console.error);

mongoose.connect('mongodb://localhost/books');

/**
 * PricingMap Schema
 */
var PricingMapSchema = new Schema({
    seller: String,
    mrp: Number,
    price: Number,
    position: Number,
    inStock: Boolean,
    rating: Number,
    url: String,
    updated: { type: Date, default: Date.now },
    offer: String
});
/**
 * Book Schema
 */
var BookSchema = new Schema({
    title: String,        
    imageUrl: String,
    bestPrice: Number,
    author: String,
    description: String,
    relevanceScore: Number,
    matchesCount: Number,
    isbn10: String,
    isbn13: String,
    pricingMap: [PricingMapSchema]
});

var Book = mongoose.model('Book', BookSchema);

var newData = new Book({
            "title": "my book",
            "imageUrl": "abc",
            "bestPrice": "abc",
            "author": "abc",
            "description": "abc",
            "relevanceScore": "abc",
            "matchesCount": 1,
            "isbn10": "abc",
            "isbn13": "9788170286615",
            "pricingMap":{
                "seller": "amazon.in",
                "mrp":  "abc",
                "price":  "100",
                "position":  "1",
                "inStock":  "abc",
                "rating":  "5",
                "url":  "abc",
                "updated": "abc",
                "offer":  "abc"
            }
        })


// Custom Methods

Book.update = function(newData){
    
    var doc;
    
    // UPDATE PRICING MAP DETAILS FOR ISBN WITH CURRENT SELLER
    Book.find({ isbn13: newData.isbn13, pricingMap[0].seller: newData.pricingMap[0].seller }, function(err, doc) {
        if (err) throw err;        
        
        // update existing price of seller 
        var conditions = { doc.pricingMap[0].seller: newData.pricingMap[0].seller };
        var update = { 
            $set: { 
                doc.pricingMap[0].price: newData.pricingMap[0].price, 
                doc.pricingMap[0].inStock: newData.pricingMap[0].inStock, 
                doc.pricingMap[0].rating: newData.pricingMap[0].rating 
            }};
        var options = { upsert: true }; // adds new entry if doc doesnt exist
        model.update(conditions, update, options, callback);            
        console.log(book);
        
    });
    
    // INSERT NEW ENTRY IN THE PRICING MAP FOR EXISTING ENTRY IN DB
    Book.update(
        {"isbn13": newData.isbn13},
        {"$push": // will only add to the end of array if value is not present
            {
                {
                doc.pricingMap[0].seller = newData.pricingMap[0].seller,
                doc.pricingMap[0].mrp = newData.pricingMap[0].mrp,
                doc.pricingMap[0].price = newData.pricingMap[0].price,
                doc.pricingMap[0].position = newData.pricingMap[0].position,
                doc.pricingMap[0].inStock = newData.pricingMap[0].inStock,
                doc.pricingMap[0].rating = newData.pricingMap[0].rating,
                doc.pricingMap[0].url = newData.pricingMap[0].url,
                doc.pricingMap[0].updated = newData.pricingMap[0].updated,
                doc.pricingMap[0].offer = newData.pricingMap[0].offer
                }
            }
        } 
    );
       
    // ADD NEW ENTRY    
    newData.save(function(err, doc){
        if (err) 
            condole.log(err);
        console.log(doc);
    });
};
