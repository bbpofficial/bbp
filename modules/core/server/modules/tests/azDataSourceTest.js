'use strict';

var util = require('util');
var OperationHelper = require('apac').OperationHelper;
var parseString = require('xml2js').parseString;

var opHelper = new OperationHelper({
    awsId:     'AKIAJ3DXTSH6JPYML4ZQ',
    awsSecret: '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',
    assocId:   'bestbookspric-21',
    locale: 'IN'
});

opHelper.execute('ItemSearch', {
    'SearchIndex': 'Books',
    'Keywords': "sachin tendulkar",
    'ResponseGroup': 'ItemAttributes,Offers,Images',
                                'Condition': 'New'
})
.then(function(response) {
    parseString(response.responseBody, function(err, result) {
        //var jsonObj = JSON.stringify(result);
        var res = result.ItemSearchResponse;
        var items = res.Items[0].Item;
        
        var products = [];
        var position = 1;
        items.forEach(function(item) {
            var itemAttributes = item.ItemAttributes[0];
            var product = {
                isbn10: -1,
                isbn13: -1,
                title: itemAttributes.Title[0],
                author: itemAttributes.Author[0],
                imageURL: item.MediumImage[0].URL[0],
                bestPrice: -1,
                relevanceScore: 0,
                matchesCount: 1,
                pricingMap: [{
                    seller: "Amazon.in",
                    mrp: "",
                    price: "",
                    position: "",
                    inStock: true, // We specify to amazon to return only in stock items
                    rating: 0,  //Amazon doesn't provide rating with API
                    url: item.DetailPageURL[0]
                }]
            };
            itemAttributes.ISBN.forEach(function(isbn) {
                if(isbn.toString().length == 10) {
                    product.isbn10 = isbn;
                } else if(isbn.toString().length == 13) {
                    product.isbn13 = isbn;
                }
            });
            
            var mrp = parseInt(itemAttributes.ListPrice[0].Amount / 100); //Last two digits are decimal places
            var price = parseInt(item.OfferSummary[0].LowestNewPrice[0].Amount / 100); //Last two digits are decimal places
            product.bestPrice = price;
            product.pricingMap[0].price = price;
            product.pricingMap[0].mrp = mrp;
            product.pricingMap[0].position = position;
            
            products.push(product);
            position++;
        });
        console.log(products);
    });
})
.catch(function(err) {
    console.error("Something went wrong! ", err);
    //reject(err);
});