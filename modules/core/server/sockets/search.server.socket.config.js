'use strict';

var  mongoose = require('mongoose'),
  Books = mongoose.model('Book');
      
var amazonDS = require("../modules/amazonDataSource");
var flipkartDS = require("../modules/flipkartDataSource");
var snapdealDS = require("../modules/snapdealDataSource");
var infibeamDS = require("../modules/infibeamDataSource");
var bookaddaDS = require("../modules/bookaddaDataSource");
var sapnaonlineDS = require("../modules/sapnaonlineDataSource");
// Create search configuration
module.exports = function (io, socket, db) {
    socket.on("search:book", function(request) {
        //Load data from amazon
        amazonDS.getData(request.q).then(function(products) {
            socket.emit("results:book", {"q": request.q, "products": products, "seller": "Amazon.in"});
        }, function(error) {
            console.log("Oh! There was an error: ", error);
        });
        
        //Load data from flipkart
        flipkartDS.getData(request.q).then(function(products) {
            socket.emit("results:book", {"q": request.q, "products": products, "seller": "Flipkart.com"});
        }, function(error) {
            console.log("Oh! There was an error: ", error);
        });
        
        //Load data from snapdeal
        /*
        snapdealDS.getData(request.q).then(function(products) {
            socket.emit("results:book", {"q": request.q, "products": products, "seller": "Snapdeal.com"});
        }, function(error) {
            console.log("Oh! There was an error: ", error);
        });
        */
        
        //Load data from infibeam
        infibeamDS.getData(request.q).then(function(products) {
           socket.emit("results:book", {q: request.q, "products": products, "seller": "Infibeam.com"}); 
        }, function(error) {
            console.log("Oh! There was an error: ", error);
        });
        
        //Load data from bookadda
        bookaddaDS.getData(request.q).then(function(products) {
           socket.emit("results:book", {q: request.q, "products": products, "seller": "Bookadda.com"}); 
        }, function(error) {
            console.log("Oh! There was an error: ", error);
        });
        
        //Load data from sapnaonline
        sapnaonlineDS.getData(request.q).then(function(products) {
           socket.emit("results:book", {q: request.q, "products": products, "seller": "Sapnaonline.com"}); 
        }, function(error) {
            console.log("Oh! There was an error: ", error);
        });
    });
    
    socket.on("request:book", function(request) {
        var isbn = request.isbn; //isbn 13
        //retrieve book with isbn from database
        Books.findOne({ isbn13: isbn }, function(error, book) {
            if(error)
                console.error(error);
            socket.emit("response:book", book);
        });
    });
    
    socket.on("prices:book", function(request) {
        var book = request.book;
        book.pricingMap.forEach(function(bookStore) {
            switch(bookStore.seller) {
                case "Amazon.in":
                    //Load data from amazon
                    amazonDS.getLatestPrice(bookStore.url).then(function(price) {
                        socket.emit("latestPrice:book", {"seller": "Amazon.in", price: price});
                    }, function(error) {
                        console.log("Oh! There was an error: ", error);
                    });
                    break;
                case "Flipkart.com":
                    flipkartDS.getLatestPrice(bookStore.url).then(function(price) {
                        socket.emit("latestPrice:book", {"seller": "Flipkart.com", price: price});
                    }, function(error) {
                        console.log("Oh! There was an error: ", error);
                    });
                    break;
                case "Snapdeal.com":
                    snapdealDS.getLatestPrice(bookStore.url).then(function(price) {
                        socket.emit("latestPrice:book", {"seller": "Snapdeal.com", price: price});
                    }, function(error) {
                        console.log("Oh! There was an error: ", error);
                    });
                    break;
                case "Infibeam.com":
                    infibeamDS.getLatestPrice(bookStore.url).then(function(price) {
                        socket.emit("latestPrice:book", {"seller": "Infibeam.com", price: price});
                    }, function(error) {
                        console.log("Oh! There was an error: ", error);
                    });
                    break;
                case "Bookadda.com":
                    bookaddaDS.getLatestPrice(bookStore.url).then(function(price) {
                        socket.emit("latestPrice:book", {"seller": "Bookadda.com", price: price});
                    }, function(error) {
                        console.log("Oh! There was an error: ", error);
                    });
                    break;
                case "Sapnaonline.com":
                    sapnaonlineDS.getLatestPrice(bookStore.url).then(function(price) {
                        socket.emit("latestPrice:book", {"seller": "Sapnaonline.com", price: price});
                    }, function(error) {
                        console.log("Oh! There was an error: ", error);
                    });
                    break;
            }
        });
    });
};