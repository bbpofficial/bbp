'use strict';

// Init the application configuration module for AngularJS application
var ApplicationConfiguration = (function () {
  // Init module configuration options
  var applicationModuleName = 'bbp';
  var applicationModuleVendorDependencies = ['ngResource', 'ngAnimate', 'ngMessages', 'ui.router', 'ui.bootstrap', 'ui.utils', 'angularFileUpload', 'rzModule', 'updateMeta'];

  // Add a new vertical module
  var registerModule = function (moduleName, dependencies) {
    // Create angular module
    angular.module(moduleName, dependencies || []);

    // Add the module to the AngularJS configuration file
    angular.module(applicationModuleName).requires.push(moduleName);
  };

  return {
    applicationModuleName: applicationModuleName,
    applicationModuleVendorDependencies: applicationModuleVendorDependencies,
    registerModule: registerModule
  };
})();

'use strict';

//Start by defining the main module and adding the module dependencies
angular.module(ApplicationConfiguration.applicationModuleName, ApplicationConfiguration.applicationModuleVendorDependencies);

// Setting HTML5 Location Mode
angular.module(ApplicationConfiguration.applicationModuleName).config(['$locationProvider', '$httpProvider',
  function ($locationProvider, $httpProvider) {
    $locationProvider.html5Mode(true).hashPrefix('!');

    //$httpProvider.interceptors.push('authInterceptor');
  }
]);

angular.module(ApplicationConfiguration.applicationModuleName).run(["$rootScope", "$state", function ($rootScope, $state) {

  // Record previous state
  $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
    storePreviousState(fromState, fromParams);
  });

  // Store previous state
  function storePreviousState(state, params) {
    // only store this state if it shouldn't be ignored 
    if (!state.data || !state.data.ignoreState) {
      $state.previous = {
        state: state,
        params: params,
        href: $state.href(state, params)
      };
    }
  }
}]);

//Then define the init function for starting up the application
angular.element(document).ready(function () {
  //Fixing facebook bug with redirect
  if (window.location.hash && window.location.hash === '#_=_') {
    if (window.history && history.pushState) {
      window.history.pushState('', document.title, window.location.pathname);
    } else {
      // Prevent scrolling by storing the page's current scroll offset
      var scroll = {
        top: document.body.scrollTop,
        left: document.body.scrollLeft
      };
      window.location.hash = '';
      // Restore the scroll offset, should be flicker free
      document.body.scrollTop = scroll.top;
      document.body.scrollLeft = scroll.left;
    }
  }

  //Then init the app
  angular.bootstrap(document, [ApplicationConfiguration.applicationModuleName]);
});

'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('core');

'use strict';

// Setting up route
angular.module('core').config(['$stateProvider', '$urlRouterProvider', '$urlMatcherFactoryProvider',
  function ($stateProvider, $urlRouterProvider, $urlMatcherFactoryProvider) {

    $urlMatcherFactoryProvider.strictMode(false);
      
    // Redirect to 404 when route not found
    $urlRouterProvider.otherwise(function ($injector, $location) {
      $injector.get('$state').transitionTo('not-found', null, {
        location: false
      });
    });

    // Home state routing
    $stateProvider
    .state('home', {
      url: '/',
      templateUrl: 'modules/core/client/views/home.client.view.html'
    })
    .state('about', {
      url: '/about',
      templateUrl: 'modules/core/client/views/about.client.view.html'
    })
    .state('contact-us', {
      url: '/contact-us',
      templateUrl: 'modules/core/client/views/contact-us.client.view.html'
    })
    .state('search', {
      url: '/search/{bookName}',
      templateUrl: 'modules/core/client/views/search.client.view.html'
    })
    .state('product', {
      url: '/{bookName}/{isbn}',
      templateUrl: 'modules/core/client/views/book.client.view.html'
    })
    .state('not-found', {
      url: '/not-found',
      templateUrl: 'modules/core/client/views/404.client.view.html',
      data: {
        ignoreState: true
      }
    })
    .state('bad-request', {
      url: '/bad-request',
      templateUrl: 'modules/core/client/views/400.client.view.html',
      data: {
        ignoreState: true
      }
    })
    .state('forbidden', {
      url: '/forbidden',
      templateUrl: 'modules/core/client/views/403.client.view.html',
      data: {
        ignoreState: true
      }
    });
  }
]);
(function() {
  'use strict';

  angular
    .module('core')
    .controller('AboutController', AboutController);

  AboutController.$inject = ['$scope'];

  function AboutController($scope) {
    var vm = this;

      
    $scope.description = "Meta description";
    // About controller logic
    // ...

    init();

    function init() {
    }
  }
})();

(function() {
  'use strict';

  angular
    .module('core')
    .controller('BookController', BookController);

  BookController.$inject = ['$scope', '$state', '$stateParams', 'Socket'];

  function BookController($scope, $state, $stateParams, Socket) {
    var vm = this;
    var isbn = (isbn) ? isbn : $stateParams.isbn;
    
    // Make sure the Socket is connected
    if (!Socket.socket) {
      Socket.connect();
    }

    Socket.on('error', function (data) {
        console.error(data || 'error');
    });

    Socket.on('response:book', function(book) {
        //console.log("Received book response: ", book);
        var totalRating = 0;
        var totalStores = 0;
        book.pricingMap.forEach(function(bookStore) {
            if (bookStore.rating > 0) {
                totalRating += bookStore.rating;
                totalStores++;
            }
        });
        totalRating = (totalRating / totalStores) * 20; //100% scale
        $scope.totalRating = totalRating;
        $scope.book = book;
    });
     
    Socket.on("latestPrice:book", function(bookStore) {
        var book = $scope.book;
        for(var i = 0; i < $scope.book.pricingMap.length; i++) {
            if($scope.book.pricingMap[i].seller == bookStore.seller) {
                if(bookStore.price !== -1) {
                    $scope.book.pricingMap[i].price = bookStore.price;
                }
                $scope.reloadingPrices--;
                break;
            }
        }
    });
    
    init();

    function init() {
        console.log("Requesting book with isbn", isbn);
        Socket.emit('request:book', {isbn: isbn});
    }
      
      
    // Remove the event listener when the controller instance is destroyed
    $scope.$on('$destroy', function () {
      Socket.removeListener('error');
      Socket.removeListener('response:book');
    });
    
    function runSearch() {
      var query = $scope.q;

      $state.go('search', { bookName: query });
    }
      
    function fetchLatestPrices() {
        //Make sure previous requests are completed
        if($scope.reloadingPrices > 0) {
            return;
        }
        Socket.emit("prices:book", {book: $scope.book});
        console.log("fetching latest prices!");
        $scope.reloadingPrices = $scope.book.pricingMap.length; // Total number of sellers
    }
    
    $scope.book = {};
    $scope.runSearch = runSearch;
    $scope.reloadingPrices = 0;
    $scope.fetchLatestPrices = fetchLatestPrices;
  }
})();
(function() {
  'use strict';

  angular
    .module('core')
    .controller('ContactController', ContactController);

  ContactController.$inject = ['$scope'];

  function ContactController($scope) {
    var vm = this;

    // Contact controller logic
    // ...

    init();

    function init() {
    }
  }
})();

'use strict';

angular.module('core').controller('HomeController', ['$scope', '$state',
  function ($scope, $state) {
    function runSearch() {
      var query = $scope.q;

      $state.go('search', { bookName: query });
    }

    $scope.runSearch = runSearch;
  }
]);

(function() {
  'use strict';

  angular
    .module('core')
    .controller('SearchController', SearchController)
    .constant("_", window._);

  SearchController.$inject = ['$scope', '$state', '$stateParams', 'Socket', '$timeout'];

  function SearchController($scope, $state, $stateParams, Socket, $timeout) {
    var vm = this;
    var sellersFilter =  {options: []};
    var inStockFilter = {IsIncluded: false};
    var bookName = $stateParams.bookName;
    var liveBooks = [];
    var priceRangeFilter = 
        {
            min: -1, 
            max: -1, 
            options: {
                floor: 0, 
                ceil: 0,
                translate: function(value) {
                  return 'Rs.' + value;
                }
            }
        };
    
    $scope.Books = liveBooks;
    $scope.q = bookName;
    
    // Make sure the Socket is connected
    if (!Socket.socket) {
      Socket.connect();
    }

    Socket.on('error', function (data) {
        console.error(data || 'error');
    });
      
    // Tell NodeJS server to crawl for the current search term
    runQueryOnServer();
      
    Socket.on("results:book", function(response) {
        var query = response.q;
        var books = response.products;
        var seller = response.seller;
        if(books === null || books.length === 0) {
            var existingOption = _.find(sellersFilter.options, {
                value: seller
            });
            if (existingOption) {
                if(existingOption.loading)
                    existingOption.loading = false;
            }
            return;
        }
        console.info("Got data from seller", seller, books);
        _.each(books, function(book) {
            var isbn, matchISBN;
            //Using ISBN13 as standard
            /*if(book.isbn10 !== -1 && book.isbn10 !== "-1") {
                isbn = book.isbn10;
                matchISBN = _.some(liveBooks, {
                   isbn10: isbn
                });
            } else */
            isbn = book.isbn13;
            matchISBN = _.some(liveBooks, {
               isbn13: isbn
            });
            
            if(matchISBN) {
                // Book exists in our map
                // Add the data from `book` into pricingMap
                /*if(isbn.length === 10) {
                    existingBook = _.find(liveBooks, _.matchesProperty('isbn10', isbn));
                } else if(isbn.length === 13) { */
                var existingBook = _.find(liveBooks, _.matchesProperty('isbn13', isbn));
                console.log("Existing book ->", existingBook);
                var index = _.findIndex(liveBooks, existingBook);
                
                var newPricingEntry = {
                    seller: response.seller,
                    mrp: parseInt(book.pricingMap.mrp),
                    price: parseInt(book.pricingMap.price),
                    position: parseInt(book.pricingMap.position),
                    inStock: (book.pricingMap.inStock == "true" || book.pricingMap.inStock == true),
                    rating: parseInt(book.pricingMap.rating),
                    url: book.pricingMap.url
                };
                
                existingBook.pricingMap.push(newPricingEntry);
                existingBook.matchesCount = existingBook.pricingMap.length;
                existingBook.relevanceScore =  calculateRelevance(existingBook);
                
                liveBooks.splice(index, 1, existingBook);
            } else {
                // Book does not exist in our pricing map
                // Add new entry into it
                var newBook = {
                    isbn10: book.isbn10,
                    isbn13: book.isbn13,
                    title: book.title,
                    imageURL: book.imageUrl,
                    bestPrice: parseInt(book.pricingMap.price),
                    relevanceScore: 0,
                    matchesCount: 1,
                    pricingMap: [{
                        seller: response.seller,
                        mrp: parseInt(book.pricingMap.mrp),
                        price: parseInt(book.pricingMap.price),
                        position: parseInt(book.pricingMap.position),
                        inStock: (book.pricingMap.inStock == "true" || book.pricingMap.inStock == true),
                        rating: parseInt(book.pricingMap.rating),
                        url: book.pricingMap.url
                    }]
                };
                
                newBook.relevanceScore = calculateRelevance(newBook);
                liveBooks.push(newBook);
            }
        });
        $scope.Books = liveBooks;
        updateFilters();
    });
    
    updateFilters();
    addDefaultSellers();
    
    function updateFilters() {
        //Reset all counts
        _.each(sellersFilter.options, function(existingOption) {
            existingOption.count = 0;
        });
        
        _.each(liveBooks, function(book) {
          _.each(book.pricingMap, function(bookStore) {
            //var intBookPrice = bookStore.price.replace("Rs. ", "").replace(",", "");
            bookStore.price = parseInt(bookStore.price);
              //Price Range Filter
            if(priceRangeFilter.min === -1 || priceRangeFilter.min > bookStore.price) {
                priceRangeFilter.options.floor = bookStore.price;
                priceRangeFilter.min = bookStore.price;
            }
            if(priceRangeFilter.max === -1 || priceRangeFilter.max < bookStore.price) {
                priceRangeFilter.options.ceil = bookStore.price;
                priceRangeFilter.max = bookStore.price;
            }

            //Sellers Filter
            var existingOption = _.find(sellersFilter.options, {
                value: bookStore.seller
            });
            if (existingOption) {
                existingOption.count += 1;
                if(existingOption.loading)
                    existingOption.loading = false;
            } else {
                sellersFilter.options.push({
                    value: bookStore.seller,
                    count: 1,
                    IsIncluded: true,
                    loading: false
                });
            }
          });
        });
    }
    
    function addDefaultSellers() {
        var sellers = ["Flipkart.com", /*"Snapdeal.com",*/ "Amazon.in", "Infibeam.com", "Bookadda.com", "Sapnaonline.com"];
        _.each(sellers, function(seller) {
           sellersFilter.options.push({
                value:seller,
                count: 0,
                IsIncluded: true,
                loading: true
            }); 
        });
    }
      
    function runSearch() {
      var query = $scope.q;

      window.location.href = "//bestbooksprices.com/search/" + query;
    }
      
    function runQueryOnServer() {
        var query = $scope.q;
        
        Socket.emit('search:book', {q: query});
        console.log("Requested server for", query);
    }

    function calculateRelevance(book) {        
        var pricingMap = book.pricingMap; // c
        var matchesCount = book.pricingMap.length; // f
        
        var relevanceScore = 0;
        var positionScore = 0;
        
        for (var bookStoreIndex in pricingMap) {
            positionScore += parseInt(pricingMap[bookStoreIndex].position) || 0;
        }
        var positionScoreAverage = positionScore / matchesCount;
        relevanceScore = 1 / positionScoreAverage + (matchesCount - 1) / 2;
        
        return relevanceScore;
    }
    
    // Remove the event listener when the controller instance is destroyed
    $scope.$on('$destroy', function () {
      Socket.removeListener('error');
      Socket.removeListener('results:book');
    });

    $scope.runSearch = runSearch;

    $scope.SellersFilter = sellersFilter;
    $scope.PriceRangeFilter = priceRangeFilter;    
    $scope.InStockFilter = inStockFilter;
    
    $scope.responsiveShowFilters = false;
    $scope.toggleFilters = function() {
        $scope.responsiveShowFilters = !$scope.responsiveShowFilters;
    };
  }
})();
(function () {
  'use strict';

  angular
    .module('core')
    .directive('searchBar', searchBar);

  searchBar.$inject = [/*Example: '$state', '$window' */];

  function searchBar(/*Example: $state, $window */) {
    return {
      restrict: 'A',
      link: function(scope, element, attrs) {
        angular.element(element).autocomplete("//suggestqueries.google.com/complete/search?client=books&ds=bo");
      }
    };
  }
})();
'use strict';

/**
 * Edits by Ryan Hutchison
 * Credit: https://github.com/paulyoder/angular-bootstrap-show-errors */

angular.module('core')
  .directive('showErrors', ['$timeout', '$interpolate', function ($timeout, $interpolate) {
    var linkFn = function (scope, el, attrs, formCtrl) {
      var inputEl, inputName, inputNgEl, options, showSuccess, toggleClasses,
        initCheck = false,
        showValidationMessages = false,
        blurred = false;

      options = scope.$eval(attrs.showErrors) || {};
      showSuccess = options.showSuccess || false;
      inputEl = el[0].querySelector('.form-control[name]') || el[0].querySelector('[name]');
      inputNgEl = angular.element(inputEl);
      inputName = $interpolate(inputNgEl.attr('name') || '')(scope);

      if (!inputName) {
        throw 'show-errors element has no child input elements with a \'name\' attribute class';
      }

      var reset = function () {
        return $timeout(function () {
          el.removeClass('has-error');
          el.removeClass('has-success');
          showValidationMessages = false;
        }, 0, false);
      };

      scope.$watch(function () {
        return formCtrl[inputName] && formCtrl[inputName].$invalid;
      }, function (invalid) {
        return toggleClasses(invalid);
      });

      scope.$on('show-errors-check-validity', function (event, name) {
        if (angular.isUndefined(name) || formCtrl.$name === name) {
          initCheck = true;
          showValidationMessages = true;

          return toggleClasses(formCtrl[inputName].$invalid);
        }
      });

      scope.$on('show-errors-reset', function (event, name) {
        if (angular.isUndefined(name) || formCtrl.$name === name) {
          return reset();
        }
      });

      toggleClasses = function (invalid) {
        el.toggleClass('has-error', showValidationMessages && invalid);
        if (showSuccess) {
          return el.toggleClass('has-success', showValidationMessages && !invalid);
        }
      };
    };

    return {
      restrict: 'A',
      require: '^form',
      compile: function (elem, attrs) {
        if (attrs.showErrors.indexOf('skipFormGroupCheck') === -1) {
          if (!(elem.hasClass('form-group') || elem.hasClass('input-group'))) {
            throw 'show-errors element does not have the \'form-group\' or \'input-group\' class';
          }
        }
        return linkFn;
      }
    };
  }]);

(function () {
  'use strict';

  angular
    .module('core')
    .filter('extractBooks', extractBooks)
    .constant("_", window._);

  extractBooks.$inject = [/*Example: '$state', '$window' */];

  function extractBooks(/*Example: $state, $window */) {
    return function(products, sellersFilterOptions, priceRange, includeOutStock, scope) {

        var filtered = [];
        
        //Active sellers
        var activeSellersFilters = _.filter(sellersFilterOptions, function(sellerOption) {
            return sellerOption.IsIncluded;
        });
        
        _.each(products, function(product) {
            var matches = 0;
            var bestPrice = -1;
            var includeProduct = false; //Assume it can not be included unless it fits a price range
            _.each(product.pricingMap, function(bookStore) {
                //Price Filter
                if(bookStore.price >= priceRange.min && bookStore.price <= priceRange.max) {
                    //Book has result that fits in the price range
                    
                    //Sellers Filter
                    if(_.some(activeSellersFilters, {
                        value: bookStore.seller
                    })) {
                        if(bookStore.inStock || includeOutStock) {
                            includeProduct = true;
                            matches++;
                            if(bestPrice === -1 || bestPrice > bookStore.price) {
                                bestPrice = bookStore.price;
                            }
                        }
                    }
                }
            });
            
            if (includeProduct && activeSellersFilters.length > 0) {
                product.bestPrice = bestPrice;
                product.matchesCount = matches;
                
                filtered.push(product);
            }
        });
        
        
        //In Stock filter
        
        return filtered;
    };
  }
})();

(function () {
  'use strict';

  angular
    .module('core')
    .filter('slugify', slugify);

  slugify.$inject = [/*Example: '$state', '$window' */];

  function slugify(/*Example: $state, $window */) {
    return function (input) {
      input = input || '';  
      input = input.replace(/ /g, '-');
      input = input.toLowerCase();
      return input;
    };
  }
})();
(function () {
  'use strict';

  angular
    .module('core')
    .factory('booksDataService', booksDataService);

  booksDataService.$inject = [/*Example: '$state', '$window' */];

  function booksDataService(/*Example: $state, $window */) {
    var service = {};
    var books = [];
    var sellersFilter, priceRangeFilter, inStockFilter;
    //sample data
    var sampleBooks = [{
        isbn10: 1,
        isbn13: 1,
        title: 'Sample book #1',
        imageURL: 'http://books-a2.infibeam.com/img/bbcfedd5/018e2/63/815/P-M-B-9789382563815.jpg',
        bestPrice: 991,
        relevanceScore: Math.floor(Math.random() * 10) + 1,
        matchesCount: 2,
        pricingMap: [{
            seller: 'Amazon.in',
            price: 991,
            position: 1,
            cheapest: true,
            inStock: true
        }, {
            seller: 'Flipkart.com',
            price: 999,
            position: 1,
            cheapest: false,
            inStock: true
        }]
    }, {
        isbn10: 2,
        isbn13: 2,
        title: 'Sample book #2',
        imageURL: 'http://books-a2.infibeam.com/img/bbcfedd5/018e2/63/815/P-M-B-9789382563815.jpg',
        bestPrice: 992,
        relevanceScore: Math.floor(Math.random() * 10) + 1,
        matchesCount: 2,
        pricingMap: [{
            seller: 'Snapdeal.com',
            price: 992,
            position: 1,
            cheapest: true,
            inStock: false
        }, {
            seller: 'Flipkart.com',
            price: 999,
            position: 1,
            cheapest: false,
            inStock: false
        }]
    }, {
        isbn10: 3,
        isbn13: 3,
        title: 'Sample book #3',
        imageURL: 'http://books-a2.infibeam.com/img/bbcfedd5/018e2/63/815/P-M-B-9789382563815.jpg',
        bestPrice: 992,
        relevanceScore: Math.floor(Math.random() * 10) + 1,
        matchesCount: 2,
        pricingMap: [{
            seller: 'Snapdeal.com',
            price: 992,
            position: 1,
            cheapest: true,
            inStock: true
        }, {
            seller: 'Amazon.in',
            price: 999,
            position: 1,
            cheapest: false,
            inStock: true
        }]
    }];

    service.getData = function(query, booksArray, sellersFilterLive, priceRangeFilterLive, inStockFilterLive) {
      books = booksArray;
      sellersFilter = sellersFilterLive;
      priceRangeFilter = priceRangeFilterLive;
      inStockFilter = inStockFilterLive;
        
      //establish socket connections to retrieve data with the mentioned parameters
      return books;
    };
      
    service.getSampleData = function() {
      service.books = sampleBooks;
    };
      
    service.books = books;

    return service;
  }
})();
'use strict';

// Create the Socket.io wrapper service
angular.module('core').service('Socket', ['$state', '$timeout',
  function ($state, $timeout) {
    // Connect to Socket.io server
    this.connect = function () {
      this.socket = io();
    };
    this.connect();

    // Wrap the Socket.io 'on' method
    this.on = function (eventName, callback) {
      if (this.socket) {
        this.socket.on(eventName, function (data) {
          $timeout(function () {
            callback(data);
          });
        });
      }
    };

    // Wrap the Socket.io 'emit' method
    this.emit = function (eventName, data) {
      if (this.socket) {
        this.socket.emit(eventName, data);
      }
    };

    // Wrap the Socket.io 'removeListener' method
    this.removeListener = function (eventName) {
      if (this.socket) {
        this.socket.removeListener(eventName);
      }
    };
  }
]);
