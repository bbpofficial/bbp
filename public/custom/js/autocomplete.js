(function (a) {
    a.Autocompleter = function (c, d) {
        this.cacheData_ = {};
        this.cacheLength_ = 0;
        this.selectClass_ = "jquery-autocomplete-selected-item";
        this.keyTimeout_ = null;
        this.lastKeyPressed_ = null;
        this.lastProcessedValue_ = null;
        this.lastSelectedValue_ = null;
        this.active_ = false;
        this.finishOnBlur_ = true;
        if (!c || !(c instanceof jQuery) || c.length !== 1 || c.get(0).tagName.toUpperCase() !== "INPUT") {
            alert("Invalid parameter for jquery.Autocompleter, jQuery object with one element with INPUT tag expected");
            return
        }
        if (typeof d === "string") {
            this.options = {
                url: d
            }
        }
        else {
            this.options = d
        }
        this.options.maxCacheLength = parseInt(this.options.maxCacheLength);
        if (isNaN(this.options.maxCacheLength) || this.options.maxCacheLength < 1) {
            this.options.maxCacheLength = 1
        }
        this.options.minChars = parseInt(this.options.minChars);
        if (isNaN(this.options.minChars) || this.options.minChars < 1) {
            this.options.minChars = 1
        }
        this.dom = {};
        this.dom.$elem = c;
        if (this.options.inputClass) {
            this.dom.$elem.addClass(this.options.inputClass)
        }
        this.dom.$results = a("<div></div>").hide();
        if (this.options.resultsClass) {
            this.dom.$results.addClass(this.options.resultsClass)
        }
        this.dom.$results.css({
            position: "absolute"
        });
        a(".search-box").append(this.dom.$results);
        var b = this;
        c.keydown(function (e) {
            b.lastKeyPressed_ = e.keyCode;
            switch (b.lastKeyPressed_) {
            case 38:
                e.preventDefault();
                if (b.active_) {
                    b.focusPrev()
                }
                else {
                    b.activate()
                }
                return false;
                break;
            case 40:
                e.preventDefault();
                if (b.active_) {
                    b.focusNext()
                }
                else {
                    b.activate()
                }
                return false;
                break;
            case 9:
                if (b.active_) {
                    e.preventDefault();
                    b.selectCurrent();
                    return false
                }
                break;
            case 13:
                if (b.active_) {
                    b.selectCurrent();
                    return true
                }
                break;
            case 27:
                if (b.active_) {
                    e.preventDefault();
                    b.finish();
                    return false
                }
                break;
            default:
                b.activate()
            }
        });
        c.blur(function () {
            if (b.finishOnBlur_) {
                setTimeout(function () {
                    b.finish()
                }, 200)
            }
        })
    };
    a.Autocompleter.prototype.position = function () {
        var b = this.dom.$elem.offset();
        this.dom.$results.css({
            top: 0
            , left: 0
        })
    };
    a.Autocompleter.prototype.cacheRead = function (e) {
        var g, d, c, b, f;
        if (this.options.useCache) {
            e = String(e);
            g = e.length;
            if (this.options.matchSubset) {
                d = 1
            }
            else {
                d = g
            }
            while (d <= g) {
                if (this.options.matchInside) {
                    b = g - d
                }
                else {
                    b = 0
                }
                f = 0;
                while (f <= b) {
                    c = e.substr(0, d);
                    if (this.cacheData_[c] !== undefined) {
                        return this.cacheData_[c]
                    }
                    f++
                }
                d++
            }
        }
        return false
    };
    a.Autocompleter.prototype.cacheWrite = function (b, c) {
        if (this.options.useCache) {
            if (this.cacheLength_ >= this.options.maxCacheLength) {
                this.cacheFlush()
            }
            b = String(b);
            if (this.cacheData_[b] !== undefined) {
                this.cacheLength_++
            }
            return this.cacheData_[b] = c
        }
        return false
    };
    a.Autocompleter.prototype.cacheFlush = function () {
        this.cacheData_ = {};
        this.cacheLength_ = 0
    };
    a.Autocompleter.prototype.callHook = function (d, c) {
        var b = this.options[d];
        if (b && a.isFunction(b)) {
            return b(c, this)
        }
        return false
    };
    a.Autocompleter.prototype.activate = function () {
        var c = this;
        var b = function () {
            c.activateNow()
        };
        var d = parseInt(this.options.delay);
        if (isNaN(d) || d <= 0) {
            d = 250
        }
        if (this.keyTimeout_) {
            clearTimeout(this.keyTimeout_)
        }
        this.keyTimeout_ = setTimeout(b, d)
    };
    a.Autocompleter.prototype.activateNow = function () {
        var b = this.dom.$elem.val();
        if (b !== this.lastProcessedValue_ && b !== this.lastSelectedValue_) {
            if (b.length >= this.options.minChars) {
                this.active_ = true;
                this.lastProcessedValue_ = b;
                this.fetchData(b)
            }
        }
    };
    a.Autocompleter.prototype.fetchData = function (c) {
        if (this.options.data) {
            this.filterAndShowResults(this.options.data, c)
        }
        else {
            var b = this;
            this.fetchRemoteData(c, function (d) {
                b.filterAndShowResults(d, c)
            })
        }
    };
    a.Autocompleter.prototype.fetchRemoteData = function (d, f) {
        var e = this.cacheRead(d);
        if (e) {
            f(e)
        }
        else {
            var b = this;
            this.dom.$elem.addClass(this.options.loadingClass);
            var c = function (h) {
                var g = false;
                if (h !== false) {
                    g = b.parseRemoteData(h);
                    b.cacheWrite(d, g)
                }
                b.dom.$elem.removeClass(b.options.loadingClass);
                f(g)
            };
            a.getJSON(this.makeUrl(d), c)
        }
    };
    a.Autocompleter.prototype.setExtraParam = function (c, d) {
        var b = a.trim(String(c));
        if (b) {
            if (!this.options.extraParams) {
                this.options.extraParams = {}
            }
            if (this.options.extraParams[b] !== d) {
                this.options.extraParams[b] = d;
                this.cacheFlush()
            }
        }
    };
    a.Autocompleter.prototype.makeUrl = function (g) {
        var c = this;
        var e = this.options.paramName || "q";
        var b = this.options.url;
        var f = a.extend({}, this.options.extraParams);
        if (this.options.paramName === false) {
            b += encodeURIComponent(g)
        }
        else {
            f[e] = g
        }
        var d = [];
        a.each(f, function (h, i) {
            d.push(c.makeUrlParam(h, i))
        });
        d.push("callback=?");
        if (d.length) {
            b += b.indexOf("?") == -1 ? "?" : "&";
            b += d.join("&")
        }
        return b
    };
    a.Autocompleter.prototype.makeUrlParam = function (b, c) {
        return String(b) + "=" + encodeURIComponent(c)
    };
    a.Autocompleter.prototype.parseRemoteData = function (l) {
        var h = [];
        var e = "";
        for (var n = 0; n < l[1].length; n++) {
            e += l[1][n][0] + "\n"
        }
        l = e;
        var b = String(l).replace("\r\n", "\n");
        var k, f, g, d, c = b.split("\n");
        var m;
        for (k = 0; k < c.length; k++) {
            d = c[k].split("|");
            g = [];
            for (f = 0; f < d.length; f++) {
                g.push(unescape(d[f]))
            }
            m = g.shift();
            h.push({
                value: unescape(m)
                , data: g
            })
        }
        return h
    };
    a.Autocompleter.prototype.filterAndShowResults = function (b, c) {
        this.showResults(this.filterResults(b, c), c)
    };
    a.Autocompleter.prototype.filterResults = function (h, e) {
        var k = [];
        var c, g, j, d, m;
        var b, l, f = "";
        for (j = 0; j < h.length; j++) {
            d = h[j];
            m = typeof d;
            if (m === "string") {
                c = d;
                g = {}
            }
            else {
                if (a.isArray(d)) {
                    c = d.shift();
                    g = d
                }
                else {
                    if (m === "object") {
                        c = d.value;
                        g = d.data
                    }
                }
            }
            c = String(c);
            if (c) {
                if (typeof g !== "object") {
                    g = {}
                }
                l = String(e);
                if (!this.options.matchInside) {
                    l = "^" + l
                }
                if (!this.options.matchCase) {
                    f = "i"
                }
                b = new RegExp(l, f);
                if (b.test(c)) {
                    k.push({
                        value: c
                        , data: g
                    })
                }
            }
        }
        if (this.options.sortResults) {
            return this.sortResults(k)
        }
        return k
    };
    a.Autocompleter.prototype.sortResults = function (c) {
        var b = this;
        if (a.isFunction(this.options.sortFunction)) {
            c.sort(this.options.sortFunction)
        }
        else {
            c.sort(function (e, d) {
                return b.sortValueAlpha(e, d)
            })
        }
        return c
    };
    a.Autocompleter.prototype.sortValueAlpha = function (b, c) {
        b = String(b.value);
        c = String(c.value);
        if (!this.options.matchCase) {
            b = b.toLowerCase();
            c = c.toLowerCase()
        }
        if (b > c) {
            return 1
        }
        if (b < c) {
            return -1
        }
        return 0
    };
    a.Autocompleter.prototype.showResults = function (j, f) {
        var c = this;
        var l = a("<ul></ul>");
        var k, d, b, e, m = false
            , h = false;
        var g = j.length;
        for (k = 0; k < g; k++) {
            d = j[k];
            b = a("<li>" + this.showResult(d.value, d.data) + "</li>");
            b.data("value", d.value);
            b.data("data", d.data);
            b.click(function () {
                var i = a(this);
                c.selectItem(i)
            }).mousedown(function () {
                c.finishOnBlur_ = false
            }).mouseup(function () {
                c.finishOnBlur_ = true
            });
            l.append(b);
            if (m === false) {
                m = String(d.value);
                h = b;
                b.addClass(this.options.firstItemClass)
            }
            if (k == g - 1) {
                b.addClass(this.options.lastItemClass)
            }
        }
        this.position();
        this.dom.$results.html(l).show();
        e = this.dom.$results.outerWidth() - this.dom.$results.width();
        this.dom.$results.width(this.dom.$elem.outerWidth() - e);
        a("li", this.dom.$results).hover(function () {
            c.focusItem(this)
        }, function () {});
        if (this.autoFill(m, f)) {
            this.focusItem(h)
        }
    };
    a.Autocompleter.prototype.showResult = function (c, b) {
        if (a.isFunction(this.options.showResult)) {
            return this.options.showResult(c, b)
        }
        else {
            return c
        }
    };
    a.Autocompleter.prototype.autoFill = function (f, d) {
        var c, b, e, g;
        if (this.options.autoFill && this.lastKeyPressed_ != 8) {
            c = String(f).toLowerCase();
            b = String(d).toLowerCase();
            e = f.length;
            g = d.length;
            if (c.substr(0, g) === b) {
                this.dom.$elem.val(f);
                this.dom.$elem.trigger('input');
                this.selectRange(g, e);
                return true
            }
        }
        return false
    };
    a.Autocompleter.prototype.focusNext = function () {
        this.focusMove(+1)
    };
    a.Autocompleter.prototype.focusPrev = function () {
        this.focusMove(-1)
    };
    a.Autocompleter.prototype.focusMove = function (b) {
        var c, d = a("li", this.dom.$results);
        b = parseInt(b);
        for (var c = 0; c < d.length; c++) {
            if (a(d[c]).hasClass(this.selectClass_)) {
                this.focusItem(c + b);
                return
            }
        }
        this.focusItem(0)
    };
    a.Autocompleter.prototype.focusItem = function (c) {
        var b, d = a("li", this.dom.$results);
        if (d.length) {
            d.removeClass(this.selectClass_).removeClass(this.options.selectClass);
            if (typeof c === "number") {
                c = parseInt(c);
                if (c < 0) {
                    c = 0
                }
                else {
                    if (c >= d.length) {
                        c = d.length - 1
                    }
                }
                b = a(d[c])
            }
            else {
                b = a(c)
            }
            if (b) {
                b.addClass(this.selectClass_).addClass(this.options.selectClass)
            }
        }
    };
    a.Autocompleter.prototype.selectCurrent = function () {
        var b = a("li." + this.selectClass_, this.dom.$results);
        if (b.length == 1) {
            this.selectItem(b)
        }
        else {
            this.finish()
        }
    };
    a.Autocompleter.prototype.selectItem = function (e) {
        var d = e.data("value");
        var c = e.data("data");
        var b = this.displayValue(d, c);
        this.lastProcessedValue_ = b;
        this.lastSelectedValue_ = b;
        this.dom.$elem.val(b).focus();
        this.dom.$elem.trigger('input');
        this.setCaret(b.length);
        this.callHook("onItemSelect", {
            value: d
            , data: c
        });
        this.finish()
    };
    a.Autocompleter.prototype.displayValue = function (c, b) {
        if (a.isFunction(this.options.displayValue)) {
            return this.options.displayValue(c, b)
        }
        else {
            return c
        }
    };
    a.Autocompleter.prototype.finish = function () {
        if (this.keyTimeout_) {
            clearTimeout(this.keyTimeout_)
        }
        if (this.dom.$elem.val() !== this.lastSelectedValue_) {
            if (this.options.mustMatch) {
                this.dom.$elem.val("")
                this.dom.$elem.trigger('input')
            }
            this.callHook("onNoMatch")
        }
        this.dom.$results.hide();
        this.lastKeyPressed_ = null;
        this.lastProcessedValue_ = null;
        if (this.active_) {
            this.callHook("onFinish")
        }
        this.active_ = false
    };
    a.Autocompleter.prototype.selectRange = function (e, b) {
        var d = this.dom.$elem.get(0);
        if (d.setSelectionRange) {
            d.focus();
            d.setSelectionRange(e, b)
        }
        else {
            if (this.createTextRange) {
                var c = this.createTextRange();
                c.collapse(true);
                c.moveEnd("character", b);
                c.moveStart("character", e);
                c.select()
            }
        }
    };
    a.Autocompleter.prototype.setCaret = function (b) {
        this.selectRange(b, b)
    };
    a.fn.autocomplete = function (b) {
        if (typeof b === "string") {
            b = {
                url: b
            }
        }
        var c = a.extend({}, a.fn.autocomplete.defaults, b);
        return this.each(function () {
            var e = a(this);
            var d = new a.Autocompleter(e, c);
            e.data("autocompleter", d)
        })
    };
    a.fn.autocomplete.defaults = {
        paramName: "q"
        , minChars: 1
        , loadingClass: "acLoading"
        , resultsClass: "acResults"
        , inputClass: "acInput"
        , selectClass: "acSelect"
        , mustMatch: false
        , matchCase: false
        , matchInside: true
        , matchSubset: true
        , useCache: false
        , maxCacheLength: 10
        , autoFill: false
        , sortResults: true
        , sortFunction: false
        , onItemSelect: false
        , onNoMatch: false
    }
})(jQuery);